<?php
	include_once("_class/db.php");
	include_once("_class/card.php");

	function generateCards($room, $user_qty, $card_qty) {
		/* Generate deck */
		$deck = array();

		for($rank = 0; $rank < 10; $rank++) {
			for($suit = 0; $suit < 4; $suit++) {
				$deck[] = new Card($rank, $suit);
			}
		}

		/* Shuffle deck */
		shuffle($deck);


		/* Give cards to users */
		$db = new db;
		$db->set_connect_db();

		for($user = 0; $user < $user_qty; $user++) {
			for($card = 0; $card < $card_qty; $card++) {
				$q = "INSERT INTO card(card, user, room)
						VALUES ('" . $deck[$user * $card_qty + $card] . "', '" . $user . "', '" . $room . "')";
				$db->query($q);
			}
		}

		$db->close();
	}
?>