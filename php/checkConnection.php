<?php
	include_once ("_class/db.php");
	
	$db = new db;
	$db->set_connect_db();

	$return = [];
	
	session_start();
	
	//Checa se j� tem sess�o
	if(isset($_SESSION['id']))
	{
		$q = "SELECT room FROM user WHERE id = " . $_SESSION['id'];
		$r = $db->query($q);
		
		//Se j� existe na tabela, update time
		if($r->num_rows != 0)
		{
			$row = $r->fetch_assoc();
			
			$timenow = time();
			$q = "UPDATE user SET time = ". $timenow ." WHERE id = " . $_SESSION['id'];
			$db->query($q);
			
			$return["success"] = 1;
			$return["room"] = intval($row["room"]);
			
			$q = "SELECT COUNT(*) AS in_game FROM room WHERE room = " . $return["room"];
			$r = $db->query($q);
			
			$row = $r->fetch_assoc();
			
			if($row["in_game"] == 1)
			{
				$return["in_game"] = 1;
			}
			else
			{
				$return["in_game"] = 0;
			}
			
		}
		//Se n�o existe na tabela, desfaz sess�o
		else
		{
			unset($_SESSION['id']);
			$return["success"] = 0;
		}
	}
	//Se n�o tem sess�o, desconecta
	else
	{
		$return["success"] = 0;
	}
	
	echo json_encode($return);
	session_write_close();
	
	//Checa geral
	$t = time() - 30;
	$q = "DELETE FROM user WHERE time < " . $t;
	$db->query($q);
	
	$db->close();
?>