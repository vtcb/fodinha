<?php
	include_once ("_class/db.php");
	include_once ("generateCards.php");

	$db = new db;
	$db->set_connect_db();

	$room = $_POST["room"];
	$ready = $_POST["ready"];
	
	if($ready == 1)
	{
		//Atualiza ready no DB
		session_start();
		$q = "UPDATE user SET ready = 1 WHERE id = " . $_SESSION['id'];
		$db->query($q);
		session_write_close();

		//Conta quantas pessoas tem na sala
		$q = "SELECT COUNT(*) AS cnt FROM user WHERE room = " . $room;
		$r = $db->query($q);

		$connected_count = $r->fetch_assoc();
		$connected_count = $connected_count["cnt"];

		//Conta quantos estão ready
		$q = "SELECT COUNT(*) AS cnt FROM user WHERE ready = 1 AND room = " . $room;
		$r = $db->query($q);

		$ready_count = $r->fetch_assoc();
		$ready_count = $ready_count["cnt"];

		if($connected_count == $ready_count && $ready_count > 1) {
			//Checa o numero de pessoas que vão jogar
			$q1 = "SELECT id FROM user WHERE room = " . $room;
			$r = $db->query($q1);
			
			$id_array = [];
			$player_qty = $r->num_rows;
			
			while($row = $r->fetch_assoc())
			{
				$id_array[] = intval($row['id']);
			}
			
			$json_array = json_encode($id_array);
			
			//Cria a sala
			$q2 = "INSERT INTO room (room, current_player, match_number, start_match, round_number, start_round, player_order, players_qty)
				VALUES (" . $room . ", 0, 2, 0, 0, 0, '" . $json_array . "', " . $player_qty . ")";
			$db->query($q2);
			
			generateCards($room, $player_qty, 2);

			echo 1;
		}
		else
		{
			$q = "DELETE FROM room WHERE room = " . $room;
			$db->query($q);
			
			$q = "DELETE FROM card WHERE room = " . $room;
			$db->query($q);
			
			echo 0;
		}
	}
	else
	{
		session_start();
		$q = "UPDATE user SET ready = 0 WHERE id = " . $_SESSION['id'];
		$db->query($q);
		session_write_close();
		
		$q = "DELETE FROM room WHERE room = " . $room;
		$db->query($q);
		
		$q = "DELETE FROM card WHERE room = " . $room;
		$db->query($q);
		
		echo 0;
	}

	$db->close();
?>