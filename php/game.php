<?php
	include_once("_class/db.php");
	include_once("roundResult.php");
	include_once("generateCards.php");

	// Parameters
	$room = $_POST["room"];
	$card = $_POST["card"];
	$bet = $_POST["bet"];

	session_start();
		$session_id = $_SESSION['id'];
	session_write_close();

	$result = [];

	/* Game state */
	$db = new db;
	$db->set_connect_db();

	$q = "SELECT * FROM room WHERE room = " . $room;
	$r = $db->query($q);


	/* Restore some info */
	$game = $r->fetch_assoc();
	
	$order = json_decode($game["player_order"]);
	$player_id = array_search($session_id, $order);

	$result["match_number"] = $game["match_number"];
	$result["bet"] = $bet;
	$result["untie"] = -1;
	
	/* If it's betting phase and my turn */
	if($game["phase"] == 0 && $player_id == $game["current_player"])
	{
		// Bet done
		if($bet != -1)
		{
			$q = "UPDATE user SET bet = " . $bet . " WHERE id = " . $session_id;
			$db->query($q);
			
			$player_id = ($player_id + 1) % $game["players_qty"];
			
			if($player_id == $game["start_match"]) 
			{
				//End betting phase
				$q = "UPDATE room SET 
				phase = 1,
				current_player = " . $player_id . "
				WHERE
				room = " . $room;
				$db->query($q);
				
				//CHECK IF NEEDED
				$result["success"] = 3;
			}
			else
			{
				//Next Player
				$q = "UPDATE room SET 
				current_player = " . $player_id . "
				WHERE
				room = " . $room;
				$db->query($q);
				
				//Waiting
				$result["success"] = 0;
			}
		}
		// Waiting for bet selection
		else
		{
			//If last player, can't tie bet
			if( ( ($player_id + 1) % $game["players_qty"] ) == $game["start_match"])
			{
				$q = "SELECT SUM(bet) AS bets FROM user WHERE room = " . $room;
				$r = $db->query($q);
				$r = $r->fetch_assoc();
				$tie = $game["match_number"] - $r["bets"];
				
				$result["untie"] = $tie;
			}
			$result["success"] = 2;
		}
	}
	/* If it's playing phase, it is my turn, and I've chosen a card */
	elseif($game["phase"] == 1 && $player_id == $game["current_player"]) {
		//Chosen card
		if($card != "--")
		{			
			$q = "UPDATE card SET chosen = 1 WHERE card = '" . $card . "' AND room = " . $room;
			$db->query($q);


			$player_id = ($player_id + 1) % $game["players_qty"];

			// Finish round
			if($player_id == $game["start_round"]) {
				$q = "SELECT card, user FROM card WHERE chosen = 1 AND room = " . $room;
				$r = $db->query($q);

				$cards = array();

				while($row = $r->fetch_assoc()) {
					$cards[] = $row;
				}

				usort($cards, "cmpByUser");

				for($i = 0; $i < $game["players_qty"]; $i++)
				{
					$cards[$i] = $cards[$i]["card"];
				}
				
				$new_cards = array();

				for($i = $player_id; $i < $game["players_qty"]; $i++) {
					$new_cards[] = $cards[$i];
				}
				for($i = 0; $i < $player_id; $i++) {
					$new_cards[] = $cards[$i];
				}

				$cards = $new_cards;

				$result = roundResult($cards, $game["players_qty"]);
				
				$result[0] = ($result[0] + $player_id + $game["players_qty"]) % $game["players_qty"];
				
				$new_round = $game["round_number"] + 1;
				
				//Winner is result[0]
				if($result[1] == 1)
				{
					$q = "UPDATE user SET
					won = won + 1
					WHERE
					id = " . $order[$result[0]];
					$db->query($q);
				}
				
				//Clear cards
				$q = "DELETE FROM card WHERE chosen = 1 AND room = " . $room;
				$db->query($q);
				
				//If there is still rounds
				if($new_round < $game["match_number"])
				{
					$q = "UPDATE room SET 
					current_player = " . $result[0] . ",
					round_number = " . $new_round . ",
					start_round = " . $result[0] . "
					WHERE
					room = " . $room;
					$db->query($q);
				}
				//If match ended
				else
				{
					//Life count && set up players
					$q = "SELECT id, lives, won, bet FROM user WHERE room = " . $room;
					$r = $db->query($q);
					
					while ($row = $r->fetch_assoc())
					{
						$updated_lives = $row["lives"] - abs($row["bet"] - $row["won"]);
						$q2 = "UPDATE user SET
						lives = " . $updated_lives . ",
						won = 0,
						bet = 0
						WHERE
						id = " . $row["id"];
						$db->query($q2);
					}
					
					//Set up next match
					$start_match = ($game["start_match"] + 1) % $game["players_qty"];
					if($game["match_number"] + 1 < 9)
					{
						$q = "UPDATE room SET
						phase = 0,
						current_player = " . $start_match . ",
						match_number = match_number + 1,
						start_match = " . $start_match . ",
						round_number = 0,
						start_round = " . $start_match . "
						WHERE
						room = " . $room;
						$db->query($q);

						generateCards($room, $game["players_qty"], $game["match_number"] + 1);
						
					}
					else
					{
						//END GAME TODO
						$db->clear_table("user");
						$db->clear_table("room");
						$db->clear_table("card");
					}
				}
			}
			// There is still more players left
			else
			{
				$q = "UPDATE room SET 
				current_player = " . $player_id . "
				WHERE
				room = " . $room;
				$db->query($q);
			}

			$result["success"] = 1;	
		}
		else
		{
			$result["success"] = 0;
		}
	} else {
		//WAITING
		$result["success"] = 0;
	}

	echo json_encode($result);
	
	$db->close();

?>
