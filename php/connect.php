<?php
	include_once ("_class/db.php");

	
	$db = new db;
	$db->set_connect_db();

	//Recebe as informações do request
	$nick = $_POST["nick"];
	$room = $_POST["room"];
	$connect = $_POST["connect"];

	//Se quiser conectar
	if($connect == 1)
	{
		//Query pra contar o numero de pessoas na sala
		$q = "SELECT COUNT(*) AS players FROM user WHERE room = " . $room;
		$r = $db->query ($q);
		
		$row = $r->fetch_assoc();
		$data = $row["players"];
		$MAXROOMSPOTS = 5;
		
		//Query pra saber se a sala está em jogo
		$q = "SELECT COUNT(*) AS room_isrunning FROM room WHERE room = " . $room;
		$r = $db->query ($q);
		
		$row = $r->fetch_assoc();
		$has_room = $row['room_isrunning'];
		
		//Nick vazio OU se a sala > 5 OU se a sala está em jogo não entra
		if(empty($nick))
		{
			$success = 1;
		}
		elseif ($has_room != 0)
		{
			$success = 4;
		}
		elseif ($data > $MAXROOMSPOTS-1)
		{
			$success = 3;
		}
		else
		{
			//Query para achar o nick na tabela
			$q = "SELECT COUNT(*) AS numberUser FROM user WHERE nick = '" . $nick ."'";
			$r = $db->query($q);
			$row = $r->fetch_assoc();

			//Usuário novo mas já tem na tabela = Nick inválido
			if ($row['numberUser'] != 0)
			{
				$success = 2;
			}
			//Nao tem na tabela, adicionar usuário
			else
			{
				//Insere na tabela
				$q = "INSERT INTO user (id, nick, ready, room, lives, time)
				VALUES ('', '" .$nick. "' , 0," . $room .", 3," . time() . ")";
				$db->query($q);
				
				session_start();
				//Atualiza sessão
				if (!isset($_SESSION['id']) || $_SESSION['id'] != $db->get_last_id())
				{
					$_SESSION['id'] = $db->get_last_id();
				}
				session_write_close();
				
				$success = 0;
			}
		}
		
		echo $success;
	}
	//Desconectar
	else
	{
		session_start();
		$q = "DELETE FROM user WHERE id = " . $_SESSION['id'];
		$db->query($q);
		unset($_SESSION['id']);
		session_write_close();
	}
	
	$db->close();
?>