<?php

	class db{
		private	$host;
		private	$username;
		private	$pwd;
		private	$db;
		private $go;
		
		public function set_connect_db()
		{
			$this->host = "localhost";
			$this->username = "root";
			$this->pwd = "admin";
			$this->db = "dbcartas";
			
			$this->go = mysqli_connect($this->host, $this->username,$this->pwd, $this->db) or die ("Could not connect." . mysql_error());
			
			if($this->go->connect_errno > 0)
			{
				die('Unable to connect to database [' . $db->connect_error . ']');
			}
			
			$this->go->set_charset("utf8");
		}
		
		public function query($q)
		{
			if(!$result = $this->go->query($q))
			{
				die('There was an error running the query [' . $this->go->error . ']');
			}
			
			return $result;
		}
		
		public function get_last_id()
		{
			return $this->go->insert_id;
		}
		
		public function close()
		{
			$this->go->close();
		}
		
		public function clear_table($table)
		{
			$sql = "TRUNCATE " . $table;
			$this->query($sql);
		}
	}

?>