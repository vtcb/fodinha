<?php

	class Card {
		private $rank;
		private $suit;

		private static $RANK = array('4', '5', '6', '7', 'Q', 'J', 'K', 'A', '2', '3');
		private static $SUIT = array('S', 'H', 'C', 'D');

		public function __construct()
		{
			$a = func_get_args(); 
			$i = func_num_args(); 
			if(method_exists($this,$f='__construct'.$i)) { 
				call_user_func_array(array($this,$f),$a); 
			}
		}

		public function __construct1($str) {
			$this->rank = array_search($str[0], self::$RANK);
			$this->suit = array_search($str[1], self::$SUIT);
		}

		public function __construct2($rank, $suit) {
			$this->rank = $rank;
			$this->suit = $suit;
		}

		public function getRank() {
            return $this->rank;
		}

		public function getSuit() {
            return $this->suit;
		}

		public function __toString() {
			return self::$RANK[$this->rank] . self::$SUIT[$this->suit];
		}

		public function value() {
			       if($this->rank == 0 && $this->suit == 2) { /* 4   of Clubs */
				return 13;
			} else if($this->rank == 3 && $this->suit == 1) { /* 7   of Hearts */
				return 12;
			} else if($this->rank == 7 && $this->suit == 0) { /* Ace of Spades */
				return 11;
			} else if($this->rank == 3 && $this->suit == 3) { /* 7   of Diamonds */
				return 10;
			} else {
				return $this->rank;
			}
		}

		public function sortValue() {
			$ans = $this->value();
			if($ans < 10) {
				return 4 * $ans + $this->suit;
			} else {
				return 4 * $ans;
			}
		}
	}

	function cmpCard($a, $b) {
		return $a->sortValue() > $b->sortValue();
	}
	
	/*
	$arr = array(new Card(0, 2), new Card(5, 3), new Card(9, 0));

	usort($arr, cmpCard);

	for($i = 0; $i < 3; $i++) {
		echo $arr[$i], ',', $arr[$i]->sortValue(), '--';
	}
	*/

	/*
	$c = new Card(0, 2);
	
	echo $c;
	echo $c->value();
	
	$c = new Card("4C");
	
	echo $c;
	echo $c->value();
	*/