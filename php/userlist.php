<?php
    include_once ("_class/db.php");
    include_once ("_class/card.php");
    
    /* Parameters */
    $room = $_POST["room"];

    session_start();
    $session_id = $_SESSION['id'];
    session_write_close();
    
    /* Init db */
    $db = new db;
    $db->set_connect_db();

    /* Get room info */
    $q = "SELECT player_order FROM room WHERE room = " . $room;
    $r = $db->query($q);

    $game = $r->fetch_assoc();

    /* Get users info */
    $q = "SELECT id, nick, lives, won, bet, ready FROM user WHERE room = " . $room;
    $r = $db->query($q);
  
    if($game) {
        $order = json_decode($game["player_order"]);
    }

    $users  = [];
    while($row = $r->fetch_assoc()) {
        $row["card"] = "--";
        
        if($game) {
            $player_id = array_search($row["id"], $order);
            $q = "SELECT card FROM card WHERE chosen = 1 AND user = " . $player_id . " AND room = " . $room;
        
            if($card = $db->query($q)->fetch_assoc()) {
                $row["card"] = $card["card"];
            }
        }

        $users[] = $row;
    }

    /* Get user hand */
    $user_hand = [];

    
    if($game) {
		$player_id = array_search($session_id, $order);
		
        $q = "SELECT card FROM card WHERE room = " . $room . " AND user = " . $player_id;
        $r = $db->query($q);
        

        $cnt = 0;

        while($row = $r->fetch_assoc()) {
            $user_hand[] = new Card($row["card"]);
            $cnt++;
        }

        usort($user_hand, "cmpCard");

        for($i = 0; $i < $cnt; $i++) {
            $user_hand[$i] = (String) $user_hand[$i];
        }
    }

    $db->close();
    
    echo json_encode([$users, $user_hand]);
?>