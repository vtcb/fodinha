<?php
	include_once("_class/card.php");


	function roundResult($cards, $qty) {
		$winner = 0;
		$win = 1;

		for($i = 0; $i < $qty; $i++) {
			$cards[$i] = new Card($cards[$i]);
		}

		for($i = 1; $i < $qty; $i++) {
			if($cards[$i]->value() > $cards[$winner]->value()) {
				$winner = $i;
				$win = 1;
			} elseif($cards[$i]->value() == $cards[$winner]->value()) {
				$winner = $i;
				$win = 0;
			}
		}

		return array($winner, $win);
	}

	function cmpByUser($a, $b) {
		return $a["user"] > $b["user"];
	}
	
	/*
	$cards = array("4H", "7S", "7S");

	$oi = roundResult($cards, 3);
	echo $oi[0];
	echo $oi[1];
	*/
?>