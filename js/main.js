//===================================================
//==================   GLOBALS  =====================
//===================================================

var nick = "";
var connected = false;
var ready = false;
var in_game = false;
var user_room = 0;
var hand = {};
var choice = -1;
var bet = -1;


//===================================================
//===============   TIMER REQUEST  ==================
//===================================================

function timerRequest()
{
	$.post("/php/checkConnection.php",
	{
	}, function(response){
		response = JSON.parse(response);
		
		//Sucesso
		if(response["success"] == 1)
		{
			if(!connected)
			{
				$("#connection_msg").html("Conectado!!");
			}
			connected = true;
			in_game = response["in_game"];
			user_room = response["room"];
			$("#connect").html("Desconectar");
		}
		//Falhou
		else if (response["success"] == 0)
		{
			if(connected)
			{
				$("#connection_msg").html("Desconectado!!");
			}
			connected = false;
			$("#connect").html("Conectar");
		}
		
		setTimeout(function()
		{
			$("#connection_msg").html("");
		}, 3000);
	});
	
	if(connected) {
		//Atualiza lista de dentro da sala
		fetchUserlist(user_room);
		
		if(in_game) {
			$.post(
				"/php/game.php",
				{
					bet: bet,
					room: user_room,
					card: (choice == -1 ? "--" : hand[choice])
				},
				function(response) {
					response = JSON.parse(response);
					
					//Waiting for bet selection
					if(response["success"] == 2)
					{
						for (var i = 0; i <= response["match_number"];i++)
						{
							if(response["untie"] != i)
							{
								$("#bet" + i).html("[" + i + "]");
							}
						}
							
					}
					else if(response["success"] == 1) {
						
					}
					//General Waiting
					else if(response["success"] == 0)
					{
						for(var i = 0; i < 9; i++) 
						{
							$("#bet" + i).html(""); 
						}
					}
					
					choice = -1;
					bet = -1;
				}
			);
		}
	}
}


//===================================================
//=================   DOC READY =====================
//===================================================

$( document ).ready(function() {
	//===================================================
	//==================  BET EVENTS ====================
	//===================================================
	$("#bet0").click(function(){ bet = 0});
	$("#bet1").click(function(){ bet = 1});
	$("#bet2").click(function(){ bet = 2});
	$("#bet3").click(function(){ bet = 3});
	$("#bet4").click(function(){ bet = 4});
	$("#bet5").click(function(){ bet = 5});
	$("#bet6").click(function(){ bet = 6});
	$("#bet7").click(function(){ bet = 7});
	$("#bet8").click(function(){ bet = 8});
	
	for(var i = 0; i < 8; i++) 
	{
		$("#bet" + i).html(""); 
	}
	
	//===================================================
	//==================  CARD EVENTS ===================
	//===================================================
	$("#card0").click(function(){ choice = 0});
	$("#card1").click(function(){ choice = 1});
	$("#card2").click(function(){ choice = 2});
	$("#card3").click(function(){ choice = 3});
	$("#card4").click(function(){ choice = 4});
	$("#card5").click(function(){ choice = 5});
	$("#card6").click(function(){ choice = 6});
	$("#card7").click(function(){ choice = 7});
	
	timerRequest();
	var timerConnected = setInterval(timerRequest, 1000)
	
	//===================================================
	//================   BUTTON READY ===================
	//===================================================
	$("#ready_button").click( function() {
		if(connected) {
			if(!ready){
				$.post("/php/ready.php",
				{
					room : user_room,
					ready : 1
				}, function(resposta){
					ready = true;
				});
				$("#ready_button").html("Not Ready!");
			}
			else
			{
				$.post("/php/ready.php",
				{
					room : user_room,
					ready : 0
				}, function(resposta){
					ready = false;
				});
				$("#ready_button").html("Ready!");
			}
		} else {
			$("#ready_text").html("Conecte-se primeiro!");

			setTimeout( function() {
				$("#ready_text").html("");
			}, 3000);
		}
	});

	//===================================================
	//===============  CONNECT BUTTON ===================
	//===================================================
	$("#connect").click (function()
	{
		if(!connected)
		{
			nick = $("#nick").val();
			user_room = $("#rooms").val();
			
			$.post("/php/connect.php",
			{
				nick : nick,
				room : user_room,
				connect : 1
			}, function(response){
				console.log(response);
				response = parseInt (response);
				
				//Sucesso
				if(response == 0)
				{
					connected = true;
					ready = false;
					$("#connection_msg").html("Conectado!");
					$("#connect").html("Desconectar");
				}
				//Falhou = NICK VAZIO
				else if (response == 1)
				{
					connected = false;
					$("#connection_msg").html("Nick invalido");
				}
				//Falhou = NICK EM USO
				else if (response == 2)
				{
					connected = false;
					$("#connection_msg").html("Nick em uso/invalido");
				}
				//Falhou = SALA CHEIA
				else if (response == 3)
				{
					connected = false;
					$("#connection_msg").html("Sala cheia");
				}
				//Falhou = SALA EM JOGO
				else if (response == 4)
				{
					connected = false;
					$("#connection_msg").html("Sala em jogo");
				}
				
				in_game = false;
			});
		}
		else
		{
			$.post("/php/connect.php",
			{
				connect : 0
			}, function(){
				connected = false;
				ready = false;
				in_game = false;
				user_room = 0;
				
				$("#connection_msg").html("Desconectado!");
				$("#connect").html("Conectar");
				
				//Limpa lista de jogadores
				// $("#players_in").html("");
				
				//Reseta botão ready
				$("#ready_button").html("Ready!");

				//clearGame();
			});
			
		}
		
		setTimeout(function()
		{
			$("#connection_msg").html("");
		}, 3000);
	});

	
	$("#cleardb").click(function(){		
		$.post("/php/cleardb.php");
	});

});