//===================================================
//================  AUX FUNCTIONS ===================
//===================================================

function fetchUserlist(room) {
	var users = [];
	
	$.post("/php/userlist.php",
	{
		room : room
	}, function(userlist){
		var tmp = JSON.parse(userlist);
		userlist = tmp[0];
		hand = tmp[1];

		/* User List */
		var s = "";
		for (user of userlist) {
			if(user["ready"] == 0) {
				s += user["nick"] + "<br />";
			}
			else {
				s += "<mark>" + user["nick"] + "</mark>" + "<br />";
			}
		}
		
		$("#players_in").html(s);

		for(var i = 0; i < 8; i++) 
		{
			$("#card" + i).html(""); 
		}
		
		/* Card List */
		var suit;
		i = 0;
		
		for (card of hand)
		{
			switch(card[1])
			{
				case "C":
					suit = "&#x2663;";
					break;
				case "H":
					suit = "&#x2665;";
					break;
				case "S":
					suit = "&#x2660;";
					break;
				case "D":
					suit = "&#x2666;";
					break;
			}
			
			if(i == choice)
			{
				$("#card" + i).html("<mark>["+ card[0] + suit +"]</mark>");
			}
			else
			{
				$("#card" + i).html("["+ card[0] + suit +"]");
			}
			i++;
		}
		
		$("#player0").html("");
		$("#player1").html("");
		$("#player2").html("");
		$("#player3").html("");
		$("#player4").html("");
		
		/* User table */
		for(i in userlist) {
			$("#player" + i).html(
				"<td>" + userlist[i]["nick"]  + "</td>" +
				"<td>" + userlist[i]["lives"] + "</td>" +
				"<td>" + userlist[i]["won"] + "/" + userlist[i]["bet"] + "</td>" +
				"<td>" + userlist[i]["card"]  + "</td>"
			);
		}
	});
}

function clearGame() {
	for(var i = 0; i < 5; i++) { $("#player" + i).html(""); }
	for(var i = 0; i < 8; i++) { $("#bet"    + i).html(""); }
	for(var i = 0; i < 8; i++) { $("#card"   + i).html(""); }
}

function printToConsole(new_line) {
	var old_text = document.getElementById('old_console_text');
	var new_text = document.getElementById('new_console_text');

	old_text.innerHTML = new_text.innerHTML + '<br />' + old_text.innerHTML;
	new_text.innerHTML = new_line;
}